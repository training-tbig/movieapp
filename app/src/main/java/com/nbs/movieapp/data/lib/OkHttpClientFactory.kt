package com.nbs.movieapp.data.lib

import com.nbs.movieapp.BuildConfig
import okhttp3.Dispatcher
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit


/**
 * Created by ghiyatshanif on 2/23/17.
 * purpose : generate retrofit service class
 */

object OkHttpClientFactory {
    private const val DEFAULT_MAX_REQUEST = 30

    fun create(): OkHttpClient {

        val builder = OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
            .addInterceptor(Interceptor {
                val request = it.request().newBuilder()
                    .url(it.request()
                        .url()
                        .newBuilder()
                        .addQueryParameter("api_key",
                        BuildConfig.API_KEY).build())
                    .build()

                it.proceed(request)
            })

        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(interceptor).build()
        }
        val dispatcher = Dispatcher()
        dispatcher.maxRequests = DEFAULT_MAX_REQUEST
        builder.dispatcher(dispatcher)

        return builder.build()
    }
}
