package com.nbs.movieapp.data.lib

import com.nbs.movieapp.data.model.MovieItem
import com.nbs.movieapp.data.model.MovieResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface Repository {
    @GET("discover/movie?sort_by=popularity.desc")
    fun getListPopularMovie(): Call<MovieResponse>

    @GET("movie/{movie_id}")
    fun getDetailMovie(@Path("movie_id") id: String):
            Call<MovieItem>
}