package com.nbs.movieapp.data.model

import com.google.gson.annotations.SerializedName

data class MovieItem(
    @SerializedName("original_title")
    val title: String?,

    @SerializedName("poster_path")
    val poster: String?,

    @SerializedName("id")
    val id: String?,

    @SerializedName("overview")
    val overview: String?,

    @SerializedName("homepage")
    val homePage: String?,

    @SerializedName("backdrop_path")
    val backdrop: String?,

    @SerializedName("original_language")
    val originalLanguage: String?,

    @SerializedName("genres")
    val genres: MutableList<Genre>
)

data class Genre(
    @SerializedName("id")
    val id: String?,

    @SerializedName("name")
    val name: String?
)