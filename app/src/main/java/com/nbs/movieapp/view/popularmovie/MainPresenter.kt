package com.nbs.movieapp.view.popularmovie

import com.nbs.movieapp.data.model.MovieItem
import com.nbs.movieapp.domain.movie.GetPopularMovieUseCase

class MainPresenter(val mainView: MainView): IMainPresenter,
    GetPopularMovieUseCase.OnPopularMovieCallback{

    var getPopularMovieUseCase:
            GetPopularMovieUseCase ?= null

    init {
        getPopularMovieUseCase = GetPopularMovieUseCase()
        getPopularMovieUseCase?.onPopularMovieCallback = this
    }

    override fun getMovieList() {
        mainView.showHideProgressBar(true)
        getPopularMovieUseCase?.callApi()
    }

    override fun onPopularMovieSuccess(movies: List<MovieItem>?) {
        mainView.showHideProgressBar(false)
        mainView.showMovieList(movies)
    }

    override fun onPopularMovieFailed(message: String) {
        mainView.showHideProgressBar(false)
        mainView.showErrorMessage(message)
    }

}