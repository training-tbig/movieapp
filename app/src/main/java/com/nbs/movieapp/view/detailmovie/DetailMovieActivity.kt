package com.nbs.movieapp.view.detailmovie

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.nbs.movieapp.BuildConfig
import com.nbs.movieapp.R
import com.nbs.movieapp.data.model.MovieItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_movie.*

class DetailMovieActivity : AppCompatActivity(),
    DetailMovieView{

    private var detailMoviePresenter:
            IDetailMoviePresenter ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)

        supportActionBar?.apply {
            title = "Detail Movie"
        }

        val movieId =
            intent.getStringExtra("MOVIE_ID")

        detailMoviePresenter = DetailMoviePresenter(this)
        detailMoviePresenter?.getDetailMovie(movieId)
    }

    override fun showDetailMovie(movieItem: MovieItem?) {
        movieItem?.apply {
            Picasso.get()
                .load(BuildConfig.BASE_BACKDROP_IMAGE_URL
                        + backdrop).into(imgBackdrop)
            tvTitle.text = title
            tvLanguage.text = originalLanguage
            tvGenre.text = genres.joinToString {
                it.name as CharSequence
            }
            tvSynopsis.text = overview
        }
    }

    override fun showErrorMessage(message: String?) {
        Toast.makeText(this, message,
            Toast.LENGTH_LONG).show()
    }

    override fun showHideProgressBar(isShown: Boolean?) {
        isShown?.apply {
            progressBarDetil.visibility =
                if(this) View.VISIBLE else View.GONE
        }
    }
}
