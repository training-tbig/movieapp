package com.nbs.movieapp.view.popularmovie

import com.nbs.movieapp.data.model.MovieItem

interface MainView {
    fun showMovieList(movies: List<MovieItem>?)

    fun showErrorMessage(message: String)

    fun showHideProgressBar(isShown: Boolean)
}