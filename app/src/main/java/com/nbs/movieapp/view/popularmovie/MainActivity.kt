package com.nbs.movieapp.view.popularmovie

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.Toast
import com.nbs.movieapp.R
import com.nbs.movieapp.data.model.MovieItem
import com.nbs.movieapp.view.detailmovie.DetailMovieActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainView,
    MovieAdapter.OnMovieItemClickListener{

    var mainPresenter: IMainPresenter?= null
    var adapter: MovieAdapter?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.apply {
            title = "Popular Movie"
        }

        rvMovie.setHasFixedSize(true)
        rvMovie.layoutManager =
            GridLayoutManager(this, 2)

        adapter = MovieAdapter()
//        adapter?.onMovieItemClickListener = this
        adapter?.setOnItemClickListener {
            val intent = Intent(this,
                DetailMovieActivity::class.java)
            intent.putExtra("MOVIE_ID", it.id)
            startActivity(intent)
        }

        adapter?.setOnLongItemClickListener {
            Toast.makeText(this, "${it.title}", Toast.LENGTH_LONG).show()
            true
        }

        rvMovie.adapter = adapter

        mainPresenter = MainPresenter(this)
        mainPresenter?.getMovieList()
    }

    override fun showMovieList(movies: List<MovieItem>?) {
        if (movies != null){
            adapter?.list?.addAll(movies.toMutableList())
            adapter?.notifyDataSetChanged()
        }
    }

    override fun showErrorMessage(message: String) {
        Toast.makeText(this, message,
            Toast.LENGTH_LONG).show()
    }

    override fun showHideProgressBar(isShown: Boolean) {
        if (isShown){
            progressBar.visibility = View.VISIBLE
        }else{
            progressBar.visibility = View.GONE
        }
    }

    override fun onMovieItemClicked(movieItem: MovieItem) {

    }
}
