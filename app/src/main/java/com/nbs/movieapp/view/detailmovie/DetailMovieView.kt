package com.nbs.movieapp.view.detailmovie

import com.nbs.movieapp.data.model.MovieItem

interface DetailMovieView {
    fun showDetailMovie(movieItem: MovieItem?)

    fun showErrorMessage(message: String?)

    fun showHideProgressBar(isShown: Boolean?)
}