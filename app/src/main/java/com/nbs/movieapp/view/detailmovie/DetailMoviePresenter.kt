package com.nbs.movieapp.view.detailmovie

import com.nbs.movieapp.data.model.MovieItem
import com.nbs.movieapp.domain.movie.GetDetailMovieUseCase

class DetailMoviePresenter(val detailMovieView: DetailMovieView):
    IDetailMoviePresenter,
    GetDetailMovieUseCase.OnGetDetailMovieCallback{

    var getDetailMovieUseCase: GetDetailMovieUseCase ?= null

    init {
        getDetailMovieUseCase = GetDetailMovieUseCase()
        getDetailMovieUseCase?.onGetDetailMovieCallback = this
    }

    override fun getDetailMovie(movieId: String) {
        detailMovieView.showHideProgressBar(true)
        getDetailMovieUseCase?.movieId = movieId
        getDetailMovieUseCase?.callApi()
    }

    override fun onGetDetailMovieSuccess(movieItem: MovieItem?) {
        detailMovieView.showHideProgressBar(false)
        detailMovieView.showDetailMovie(movieItem)
    }

    override fun onGetDetailMovieFailed(message: String) {
        detailMovieView.showHideProgressBar(false)
        detailMovieView.showErrorMessage(message)
    }
}