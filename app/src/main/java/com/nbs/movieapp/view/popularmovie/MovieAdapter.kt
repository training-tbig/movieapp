package com.nbs.movieapp.view.popularmovie

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nbs.movieapp.BuildConfig
import com.nbs.movieapp.R
import com.nbs.movieapp.data.model.MovieItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie_list.view.*

class MovieAdapter:
    RecyclerView.Adapter<MovieAdapter.MovieViewholder>() {

    val list = mutableListOf<MovieItem>()

//    var onMovieItemClickListener: OnMovieItemClickListener ?= null

    var onMovieItemClickListener: ((MovieItem) -> Unit) ?= null

    var onMovieLongItemClickListener: ((MovieItem) -> Boolean) = { it -> false }

    fun setOnItemClickListener(func: (MovieItem) -> Unit){
        this.onMovieItemClickListener = func
    }

    fun setOnLongItemClickListener(func: (MovieItem) -> Boolean){
        this.onMovieLongItemClickListener = func
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup,
                                    position: Int): MovieViewholder {
        val view = LayoutInflater
            .from(viewGroup.context)
            .inflate(R.layout.item_movie_list, viewGroup,
                false)

        return MovieViewholder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(viewholder: MovieViewholder, position: Int) {
        viewholder.bind(list[position])
    }

    inner class MovieViewholder(itemView: View)
        : RecyclerView.ViewHolder(itemView) {

        fun bind(movieItem: MovieItem){
            Picasso.get()
                .load(BuildConfig.BASE_IMAGE_URL +
                        movieItem.poster)
                .into(itemView.imgMovie)

            itemView.imgMovie.setOnClickListener {
//                onMovieItemClickListener?
//                .onMovieItemClicked(movieItem)
                onMovieItemClickListener?.invoke(movieItem)
            }

            itemView.imgMovie.setOnLongClickListener {
                onMovieLongItemClickListener.invoke(movieItem)
            }
        }
    }

    interface OnMovieItemClickListener{
        fun onMovieItemClicked(movieItem: MovieItem)
    }
}