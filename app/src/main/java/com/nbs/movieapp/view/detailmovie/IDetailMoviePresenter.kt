package com.nbs.movieapp.view.detailmovie

interface IDetailMoviePresenter{
    fun getDetailMovie(movieId: String)
}