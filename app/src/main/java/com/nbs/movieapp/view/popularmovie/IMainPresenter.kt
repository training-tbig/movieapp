package com.nbs.movieapp.view.popularmovie

interface IMainPresenter {
    fun getMovieList()
}