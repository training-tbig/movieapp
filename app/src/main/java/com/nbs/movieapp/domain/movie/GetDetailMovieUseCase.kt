package com.nbs.movieapp.domain.movie

import com.nbs.movieapp.data.lib.UseCase
import com.nbs.movieapp.data.model.MovieItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GetDetailMovieUseCase: UseCase() {

    var onGetDetailMovieCallback:
            OnGetDetailMovieCallback ?= null
    var movieId: String ?= null

    override fun callApi() {
        val request = movieId?.let {
            apiRequest
                .getDetailMovie(it)
        }
        request?.enqueue(object: Callback<MovieItem>{
            override fun onResponse(call: Call<MovieItem>, response: Response<MovieItem>) {
                if (response.isSuccessful){
                    onGetDetailMovieCallback?.
                        onGetDetailMovieSuccess(response.body())
                }else{
                    onGetDetailMovieCallback?.
                        onGetDetailMovieFailed("Request gagall")
                }
            }

            override fun onFailure(call: Call<MovieItem>, t: Throwable) {
                onGetDetailMovieCallback?.
                    onGetDetailMovieFailed("Request gagal")
            }
        })
    }

    interface OnGetDetailMovieCallback{
        fun onGetDetailMovieSuccess(movieItem: MovieItem?)

        fun onGetDetailMovieFailed(message: String)
    }
}