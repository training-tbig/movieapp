package com.nbs.movieapp.domain.movie

import com.nbs.movieapp.data.lib.UseCase
import com.nbs.movieapp.data.model.MovieItem
import com.nbs.movieapp.data.model.MovieResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GetPopularMovieUseCase: UseCase() {

    var onPopularMovieCallback: OnPopularMovieCallback ?= null

    override fun callApi() {
        val request =
            apiRequest.getListPopularMovie()
        request.enqueue(object: Callback<MovieResponse>{
            override fun onResponse(call: Call<MovieResponse>,
                                    response: Response<MovieResponse>) {
                if (response.isSuccessful){
                    onPopularMovieCallback?.
                            onPopularMovieSuccess(response
                                .body()?.result)
                }else{
                    onPopularMovieCallback?.
                        onPopularMovieFailed("Request gagal")
                }
            }

            override fun onFailure(call: Call<MovieResponse>,
                                   t: Throwable) {
                onPopularMovieCallback?.
                        onPopularMovieFailed("Request gagal")
            }
        })
    }

    interface OnPopularMovieCallback{
        fun onPopularMovieSuccess(movies: List<MovieItem>?)

        fun onPopularMovieFailed(message: String)
    }
}